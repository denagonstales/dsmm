## About
DSMM is a manager for Minecraft dedicated server.

DSMM works on Linux and is licensed under the GNU General Public License 2.

## Installation
To install DSMM you have to drag the file DSMM.py and config.yml in some directory.
After this you have to configure it or it won't works.

## How it works
When you start it you have to choose what to do and the program will do everything.

## Requirements
* Linux.
* Python2.
* [Screen](https://www.gnu.org/software/screen/).
* [PyYaml](http://pyyaml.org/).

## Help needed
You can help me by [proposing improvements or reporting bugs](https://bitbucket.org/denagonstales/dsmm/issues).

## License
Nips is licensed under the GNU General Public license 2. See [LICENSE](LICENSE) for more information.

## Project Status & Download
DSMM's current version is **1.0**. You can download the latest release from [here](https://bitbucket.org/denagonstales/dsmm/downloads). Otherwise you can get the latest development version by cloning this repository.
